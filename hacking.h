#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <strings.h>
#include <pcap.h>


#define ETHER_ADDR_LEN 6
#define ETHER_HDR_LEN 14

#define RAW_ROW_LENGTH 38
#define WHITE_SPACE 32
#define BYTE_ASCII_DELIM "| "

struct ether_header {
    unsigned char ether_dest_addr[ETHER_ADDR_LEN];
    unsigned char ether_src_addr[ETHER_ADDR_LEN];
    unsigned short ether_type;
};

struct ip_hdr {
    unsigned char ip_version_and_header_length; // Version and length of header length
    unsigned char ip_tos;						// Type of service
    unsigned short ip_len;						// Total length
	unsigned short ip_id;						// Identifier of packet
	unsigned short ip_flag_offset;				// Flags 3 bits offset 13 bits
	unsigned char ip_ttl;						// Time  alive
	unsigned char ip_type;						// Protocol
	unsigned short ip_checksum;					// Control sum
	unsigned int ip_src_addr;					// Source address
	unsigned int ip_dest_addr;					// Destination address
};


void fill_row(const int *i) {
	unsigned char prtd_bytech_count = (unsigned char)(*i % 16) * 2;
	unsigned char prtd_whitesp_count = (unsigned char)(*i % 16) / 2;
	for(unsigned char j = 0; j < RAW_ROW_LENGTH - ( prtd_bytech_count + prtd_whitesp_count); j++) {
		printf("%c", WHITE_SPACE);
	} 
}

char *IPAddressToString(int ip, char *result)
{
  sprintf(result, "%d.%d.%d.%d",
    (ip      ) & 0xFF,
    (ip >>  8) & 0xFF,
    (ip >> 16) & 0xFF,
    (ip >> 24) & 0xFF);

  return result;
}

void decode_ethernet(const unsigned char *packet) {
    const struct ether_header *header;
    header = (const struct ether_header *)packet;

    printf("[[ Layer 2 :: ethernet header ]] ");
    printf("[ Source: %02x", header->ether_src_addr[0]);
    for(int i = 1; i < ETHER_ADDR_LEN; i++ ) {
      printf(":%02x", header->ether_src_addr[i]);
    }
    printf("\tDest: %02x", header->ether_dest_addr[0]);
    for(int i = 1; i < ETHER_ADDR_LEN; i++ ) {
  	    printf(":%02x", header->ether_dest_addr[i]);
    }
    printf("\tType: %hu]\n", header->ether_type);
}

void decode_ip(const u_char *packet) {
	const struct ip_hdr *ip_header;
    ip_header = (const struct ip_hdr *)packet;

	
	printf("\t[[ Layer 3 ::: ip header ]] \n");
    
    char res[16];
    
    printf("\t\tip_version_and_header_length: %u\n", ip_header->ip_version_and_header_length);
    printf("\t\tip_tos: 0x%02x\n", ip_header->ip_tos);
    printf("\t\tip_len: 0x%04x\n", ip_header->ip_len);
    printf("\t\tip_id: 0x%04x\n", ip_header->ip_id);
    printf("\t\tip_flag_offset: 0x%04x\n", ip_header->ip_flag_offset);
    printf("\t\tip_ttl: 0x%02x\n", ip_header->ip_ttl);
    printf("\t\tip_type: 0x%02x\n", ip_header->ip_type);
    printf("\t\tip_checksum: 0x%04x\n", ip_header->ip_checksum);
    printf("\t\t[ ip_src_addr: 0x%08x ] [ %s ]\n", ip_header->ip_src_addr, IPAddressToString(ip_header->ip_src_addr, res));
    printf("\t\t[ ip_dest_addr: 0x%08x ] [ %s ]\n", ip_header->ip_dest_addr,  IPAddressToString(ip_header->ip_dest_addr, res));
}

void dump(const unsigned char *buffer, int length) {
	unsigned char byte;

	printf("\t0x%04x%c", 0, WHITE_SPACE);
	for(int i = 0; i < length; i++) {
		if(( i % 2) == 0 ) {
			printf("%c", WHITE_SPACE);
		}
		printf("%02x", buffer[i]);

		if((i % 16 == 15) || (i == length -1)) {
			fill_row(&i);

			printf("%s", BYTE_ASCII_DELIM);
			for(int j = i - (i % 16); j <= i; j++) {
				byte = buffer[j];
				if((byte > 37) && (byte < 127)) {
					printf("%c", byte);
				} else { 
					printf("."); 
				}
			}
			printf("\n");
			if(!(i == length - 1))
				printf("\t0x%04x%c", i + 1, WHITE_SPACE);
		}
	}
}
