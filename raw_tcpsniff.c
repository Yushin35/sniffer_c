#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>

#include "hacking.h"

int main() {
	int i, recv_len, sockfd;
	unsigned long int total_bytes = 0;
	u_char buffer[9000];
	
	if((sockfd = socket(PF_INET, SOCK_RAW, IPPROTO_TCP)) == -1) {
		fprintf(stderr, "Cannot create socket\n");
		exit(1);
    }

    for(;;) {
		recv_len = recv(sockfd, buffer, 8000, 0);
		printf("RECV: %u bytes\n", recv_len);
		dump(buffer, recv_len);
		bzero(buffer, 8001);
    }
}
