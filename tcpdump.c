#include "hacking.h"

void pcap_fatal(const char *, const char *err_buf);
void process_packet(
    unsigned char *,
    const struct pcap_pkthdr *, 
    const unsigned char *
);

int main() {
    struct pcap_pkthdr cap_header;
    const u_char *packet, *pkt_data;
    char errbuf[PCAP_ERRBUF_SIZE];
    char *device;
    pcap_t *pcap_handle;
    
    device = pcap_lookupdev(errbuf);
    if(device == NULL)
        pcap_fatal("pcap_lookupdev", errbuf);
    
    pcap_handle = pcap_open_live(device, 4096, 1, 0, errbuf);
    if(pcap_handle == NULL)
        pcap_fatal("pcap_open_live", errbuf);
    
    printf("Listen packets on device: %s\n", device);
    pcap_loop(pcap_handle, 90000000, process_packet, NULL);
    
    pcap_close(pcap_handle);

}


void pcap_fatal(const char *msg, const char *err_buf) {
    printf("Erro in %s: %s\n", msg, err_buf);
    exit(1);
}
void process_packet(
    u_char *user_args,
    const struct pcap_pkthdr *cap_header, 
    const u_char *packet
) 
{
    printf("RECV %u bytes\n", cap_header->len);
    
    dump(packet, cap_header->len);
    
    decode_ethernet(packet);
    decode_ip(packet + ETHER_HDR_LEN);
}


